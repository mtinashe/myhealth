curl -X POST -H "Content-Type: application/json" -d '{
"persistent_menu":[
    {
    "locale":"default",
    "composer_input_disabled":true,
    "call_to_actions":[
        {
        "title":"MyHealth Actions",
        "type":"nested",
        "call_to_actions":[
            {
            "title":"Subscribe",
            "type":"postback",
            "payload":"subscribe"
            },
            {
            "title":"Unsubscribe",
            "type":"postback",
            "payload":"unsubscribe"
            }
        ]}
        ] 
    }
]
}' "https://graph.facebook.com/v2.6/me/messenger_profile?access_token=EAANzYJJcD4kBADwkel362AZBJZCZBeGvNB9cXtrun5STrXZBxwlC5n3XKq2rSHFbGXBeCvOYZBcN2A079FZA9ZBBBwzmaROk7ZCqHqg8XQ1AwMCffOQ0lF4iYSbHfspfIYvv2ECGGSlah94Ak8WqiLPA9tsf4rLdBcusDUHlJuLwTLifZA8AZAqX0f"