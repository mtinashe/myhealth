#!/bin/bash

# initwelcome.sh: sets welcome screen
# Execute once only

# Greeting Text
curl -X POST -H "Content-Type: application/json" -d '{
  "setting_type":"greeting",
  "greeting":{
    "text":"Hi {{user_full_name}}, welcome to MyHealth your companion to daily health tips."
  }
}' "https://graph.facebook.com/v2.6/me/thread_settings?access_token=EAANzYJJcD4kBADwkel362AZBJZCZBeGvNB9cXtrun5STrXZBxwlC5n3XKq2rSHFbGXBeCvOYZBcN2A079FZA9ZBBBwzmaROk7ZCqHqg8XQ1AwMCffOQ0lF4iYSbHfspfIYvv2ECGGSlah94Ak8WqiLPA9tsf4rLdBcusDUHlJuLwTLifZA8AZAqX0f"

# Get Started button
curl -X POST -H "Content-Type: application/json" -d '{
  "setting_type":"call_to_actions",
  "thread_state":"new_thread",
  "call_to_actions":[
    {
      "payload":"Get Started"
    }
  ]
}' "https://graph.facebook.com/v2.6/me/thread_settings?access_token=EAANzYJJcD4kBADwkel362AZBJZCZBeGvNB9cXtrun5STrXZBxwlC5n3XKq2rSHFbGXBeCvOYZBcN2A079FZA9ZBBBwzmaROk7ZCqHqg8XQ1AwMCffOQ0lF4iYSbHfspfIYvv2ECGGSlah94Ak8WqiLPA9tsf4rLdBcusDUHlJuLwTLifZA8AZAqX0f"