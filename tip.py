import pymongo
import myhealthserver as my
import time

uri = os.getenv('MONGO_URI')
client = pymongo.MongoClient(uri)
database = client['myhealth']

start_time = time.time()
PAT = my.PAT

'''This function reads the recipient
   id's and the tips to be sent from
   the database '''

print("Running periodic task")

counter = database['count']

count_string = counter.find_one({'id': "counter"})
count = int(count_string['count']) + 1

counter.update({'id':'counter'}, {"$set":{'count': count}})
recipient = database['subscribers']
tips = database['tips']
tip = tips.find_one({'id': count})
tip_content = str.encode(tip['tip'])

recipient_ids = [id['SenderID'] for id in recipient.find({})]



for id in recipient_ids:
    my.send_message(PAT, id, tip_content)

print("Elapsed time: ", str(time.time() - start_time))