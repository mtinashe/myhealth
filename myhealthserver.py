from flask import Flask, request
from flask_pymongo import PyMongo
import datetime
import json
import requests
import random
import os

app = Flask(__name__)
app.config['MONGO_DBNAME'] = "myhealth"
app.config['MONGO_URI'] = os.getenv('MONGO_URI')

mongo = PyMongo(app)

PAT = os.getenv('PAT')
@app.route('/', methods=['GET'])
def verify():
  print("Handling Verification.")
  if request.args.get('hub.verify_token', '') == 'growing_beyond':
    print ("Verification successful!")
    return request.args.get('hub.challenge', '')
  else:
    print ("Verification failed!")
    return 'Error, wrong validation token'

@app.route('/', methods=['POST'])
def webhook():
        print("Handling Messages")
        payload = request.get_data()
        print(payload)

        for sender, message in messaging_events(payload):
            if message == b'Get Started':
                received_postback(message,sender)
            elif message == b'subscribe':
                received_postback(message,sender)
            elif message == b'unsubscribe':
                received_postback(message,sender)
            elif message == b'Hie':
                print("Incoming from %s: %s" % (sender, message))
                resp = b'Hie how are you'
                send_message(PAT, sender, resp)
            elif message == b'subscribe':
                resp = b'You have subscribed successfully'
                send_message(PAT, sender, resp)
            else:
                pass

        return "ok"


def received_postback(message, sender,date = datetime.datetime.utcnow()):
    sender_id = sender
    date_of_reg = date
    payload = message
    sender_db = mongo.db.subscribers
    tips = mongo.db.tips

    print ("received postback with payload {payload}".format(payload=payload))

    if payload == b'Get Started':
        resp =  b'Welcome to MyHealth You have been automatically subscribed to receive free health tips, you can always unsubscribe by replying with unsubscribe'
        send_message(PAT, sender_id,resp)
        print("Adding to database")
        sender_db.insert({"SenderID":sender_id, "DOR":date_of_reg})
        tip_ms = b'To get you going allow me to send you a tip to carry you through the day'
        send_message(PAT, sender_id, tip_ms)
        tip = tips.find_one({'id':8})
        tip_content = str.encode(tip['tip'])
        send_message(PAT, sender_id, tip_content)
    elif payload == b'subscribe':
        if sender_db.find_one({'SenderID': sender_id}) == None:
            sender_db.insert({'SenderID':sender_id, 'DOR': date_of_reg})
            resp = b'You have been subscribed successfully'
            send_message(PAT, sender_id, resp)
            tip_ms = b'To get you going allow me to send you a tip to carry you through the day'
            send_message(PAT, sender_id, tip_ms)
            tip = tips.find_one({'id': 1})
            tip_content = str.encode(tip['tip'])
            send_message(PAT, sender_id, tip_content)
        else:
            resp = b'Subscribed Successfully'
            send_message(PAT, sender_id, resp)
    elif payload == b'unsubscribe':
        sender_db.remove({'SenderID': sender_id})
        resp = b'So sad to see you go. BYE!!'
        send_message(PAT, sender_id, resp)
    else:
        resp = b"Successful thanks"
        send_message(PAT, sender_id, resp)


def messaging_events(payload):
  """Generate tuples of (sender_id, message_text) from the
       provided payload.
    """
  data = json.loads(payload)
  messaging_events = data["entry"][0]["messaging"]
  for event in messaging_events:
    if "message" in event and "text" in event["message"]:
      yield event["sender"]["id"], event["message"]["text"].encode('unicode_escape')
    elif "postback" in event and "payload" in event["postback"]:
      yield event["sender"]["id"], event["postback"]["payload"].encode('unicode_escape')
    else:
      yield event["sender"]["id"], "I can't echo this"


def send_message(token, recipient, text):
  """Send the message text to recipient with id recipient.
  """

  r = requests.post("https://graph.facebook.com/v2.6/me/messages",
    params={"access_token": token},
    data=json.dumps({
      "recipient": {"id": recipient},
      "message": {"text": text.decode('unicode_escape')}
    }),
    headers={'Content-type': 'application/json'})
  if r.status_code != requests.codes.ok:
    print(r.text)


def send_tip_message(token, recipient, text):
    """Send the message text to recipient with id recipient.
      """
    r = requests.post("https://graph.facebook.com/v2.6/me/messages",
                      params={"access_token": token},
                      data=json.dumps({
                          "recipient": {"id": recipient},
                          "message": {"text": text.decode('unicode_escape'),
                                      "quick_replies": [
                                          {
                                              "content_type": "text",
                                              "title": "Send Another",
                                              "payload": "another",
                                              "image_url": "http://www.clker.com/cliparts/T/G/b/7/r/A/red-dot-md.png"
                                          },
                                          {
                                              "content_type": "location"
                                          },
                                          {
                                              "content_type": "text",
                                              "title": "Am Done",
                                              "payload": "done"
                                          }
                                      ]
                                      },
                      }),
                      headers={'Content-type': 'application/json'})
    if r.status_code != requests.codes.ok:
        print(r.text)


if __name__ == '__main__':
    app.run()
